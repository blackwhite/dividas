from rest_framework import serializers
from dividasapi.models import Divida, Empresa, Devedor


class EmpresaSerializer(serializers.ModelSerializer):

    class Meta:
        model = Empresa
        fields = ['id', 'nome']


class DevedorSerializer(serializers.ModelSerializer):

    class Meta:
        model = Devedor
        fields = '__all__'


class DividaSerializer(serializers.ModelSerializer):
    cobradora = EmpresaSerializer(read_only=True)
    devedor = DevedorSerializer()

    class Meta:
        model = Divida
        fields = ['id', 'sobre', 'valor', 'incluida_em', 'cobradora', 'devedor']


    def create(self, validated_data):
        devedor_data = validated_data['devedor']
        cpf = devedor_data['cpf']

        devedor = Devedor.objects.filter(cpf=cpf).first()

        if not devedor:
            devedor = Devedor.objects.create(cpf=cpf)

        validated_data['devedor'] = devedor

        return Divida.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.sobre = validated_data.get('sobre', instance.sobre)
        instance.valor = validated_data.get('valor', instance.valor)

        instance.save()

        return instance
