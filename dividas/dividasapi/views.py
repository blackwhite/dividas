from django.shortcuts import render, redirect
from django.views.decorators.http import require_GET
from django.http import JsonResponse
from django.core import serializers
from dividasapi.forms import ConsultaCPF
from dividasapi.models import Devedor, Divida, Empresa
from dividasapi.serializers import DividaSerializer
from dividasapi.decorators import api_key_required
from django.shortcuts import get_object_or_404


@require_GET
def dividas(request):
    cpf = request.GET.get('cpf', None)

    if not cpf:
        resposta = {
            'mensagem': 'Parâmetro cpf não encontrado',
            'descricao': 'É necessário informar o cpf para realizar a busca por dívidas'
        }

        return JsonResponse(resposta, status=400)

    dividas = Divida.objects.filter(devedor__cpf=cpf)
    dividas_json = DividaSerializer(dividas, many=True)

    return JsonResponse(dividas_json.data, safe=False)


@require_GET
@api_key_required
def empresa_dividas(request, id):
    empresa = Empresa.objects.filter(pk=id).first()

    if not empresa:
        return JsonResponse({'mensagem': 'Empresa não encontrada'}, status=404)

    dividas = empresa.divida_set.all()
    dividas_json = DividaSerializer(dividas, many=True)

    return JsonResponse(dividas_json.data, safe=False)
