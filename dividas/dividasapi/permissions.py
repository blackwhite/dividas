from rest_framework import permissions


class IsOwnerAdminOrReadOnly(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True

        return obj.cobradora.apikey_set.first() == request.auth
