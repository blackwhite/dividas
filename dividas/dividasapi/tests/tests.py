from django.test import TestCase, TransactionTestCase
from dividasapi.forms import ConsultaCPF, DividaForm
from dividasapi.models import Devedor, Divida, Empresa

class ConsultaCPFFormTest(TestCase):

    def test_valid_cpf(self):
        form = ConsultaCPF({'cpf': '00000000000'})

        self.assertTrue(form.is_valid())

    def test_invalid_cpf(self):
        form = ConsultaCPF({'cpf': '0'})

        self.assertFalse(form.is_valid())


class DividaFormTest(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.empresa = Empresa.objects.create(nome='Metalpox')
        cls.devedor = Devedor.objects.create(cpf='33333333333')
        cls.divida_data = {
            'sobre': 'Comprou um sapato e não pagou',
            'valor': 2000.0,
            'cobradora': cls.empresa.id,
            'cpf': '12345678900',
        }

        super(DividaFormTest, cls).setUpClass()

    def test_valid_form_new(self):
        form = DividaForm(self.divida_data)

        self.assertTrue(form.is_valid())

        self.assertEquals(form.cleaned_data['cpf'], self.divida_data['cpf'])
        self.assertEquals(form.cleaned_data['valor'], self.divida_data['valor'])
        self.assertEquals(form.cleaned_data['sobre'], self.divida_data['sobre'])
        self.assertEquals(form.cleaned_data['cobradora'].id, self.divida_data['cobradora'])

    def test_save(self):
        form = DividaForm(self.divida_data)

        self.assertTrue(form.is_valid())

        divida = form.save()

        self.assertTrue(Divida.objects.filter(pk=divida.id).exists())
        self.assertTrue(divida.devedor.cpf, self.divida_data['cpf'])
        self.assertEquals(divida.valor, self.divida_data['valor'])
        self.assertEquals(divida.sobre, self.divida_data['sobre'])
        self.assertEquals(divida.cobradora.id, self.divida_data['cobradora'])

    def test_form_without_data(self):
        form = DividaForm({})
        erros = {
            'valor': ['Este campo é obrigatório.'],
            'cobradora': ['Este campo é obrigatório.'],
            'cpf': ['Este campo é obrigatório.'],
            'sobre': ['Este campo é obrigatório.']
            }

        self.assertFalse(form.is_valid())
        self.assertEquals(form.errors, erros)

    def test_form_with_errors(self):
        data = dict(self.divida_data)
        del data['valor']

        form = DividaForm(data)

        self.assertFalse(form.is_valid())
        self.assertEquals(form.errors,
                          {'valor': ['Este campo é obrigatório.']}
                          )
