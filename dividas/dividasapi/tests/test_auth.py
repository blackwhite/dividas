from rest_framework import test
from dividasapi.models import Empresa, APIKey
from dividasapi.auth import APIKeyAuth
import pdb


class APIKeyAuthTest(test.APITestCase):

    @classmethod
    def setUpTestData(cls):
        key = APIKey.gerar()
        cls.empresa = Empresa.objects.create(nome='Metalnox')

        cls.api_key = APIKey.objects.create(api_key=key, empresa=cls.empresa)

    def setUp(self):
        self.factory = test.APIRequestFactory()
        self.auth = APIKeyAuth()

    def test_valid_api_key(self):
        request = self.factory.get('/', HTTP_X_API_KEY=self.api_key.api_key)

        user, auth = self.auth.authenticate(request)

        self.assertEquals(self.api_key.api_key, auth.api_key)
        self.assertIs(user, None)

    def test_invalid_api_key(self):
        request = self.factory.get('/', HTTP_X_API_KEY='88778878778')

        user, auth = self.auth.authenticate(request)

        self.assertIs(auth, None)
        self.assertIs(user, None)

    def test_without_api_key(self):
        request = self.factory.get('/')

        user, auth = self.auth.authenticate(request)

        self.assertIs(auth, None)
        self.assertIs(user, None)
