from django.urls import reverse
from rest_framework import test
from rest_framework import status
from dividasapi.models import Divida, Empresa, Devedor, APIKey
from dividasapi.serializers import DividaSerializer
from dividasapi.auth import APIKeyAuth
from dividasapi.api import DividaViewSet


class ListDividaAPI(test.APITestCase):

    fixtures = ['data.json']
    url_name =  'api:dividas-list'

    @classmethod
    def setUpTestData(cls):
        cls.divida = Divida.objects.get(pk=2)
        cls.api_key = APIKey.objects.get(pk=1)
        cls.qtd_dividas = Divida.objects.all()
        cls.itens_por_pagina = 20

    def setUp(self):
        self.factory = test.APIRequestFactory()

    def test_list_dividas(self):
        request = self.factory.get(reverse(self.url_name))
        view = DividaViewSet.as_view({'get': 'list'})

        response = view(request)

        dividas = response.data

        self.assertEquals(response.status_code, status.HTTP_200_OK)
        self.assertTrue(len(dividas) <= self.itens_por_pagina)


    def test_list_dividas_with_auth(self):
        headers = {'x-api-key': self.api_key.api_key}
        request = self.factory.get(reverse(self.url_name), headers)
        view = DividaViewSet.as_view({'get': 'list'})

        test.force_authenticate(request, token=self.api_key)

        response = view(request)

        dividas = response.data

        self.assertEquals(response.status_code, status.HTTP_200_OK)
        self.assertTrue(len(dividas) <= self.itens_por_pagina)

    def test_list_dividas_by_cpf(self):
        request = self.factory.get(reverse(self.url_name), {'cpf': '66666666666'})
        view = DividaViewSet.as_view({'get': 'list'})

        response = view(request)

        dividas = response.data

        self.assertEquals(response.status_code, status.HTTP_200_OK)
        self.assertEquals(len(dividas), 2)

    def test_dividas_by_invalid_cpf(self):
        request = self.factory.get(reverse(self.url_name), {'cpf': 'xxxxxx'})
        view = DividaViewSet.as_view({'get': 'list'})

        response = view(request)

        dividas = response.data

        self.assertFalse(dividas)


class GetDividaAPI(test.APITestCase):

    fixtures = ['data.json']
    url_name = 'api:dividas-detail'

    @classmethod
    def setUpTestData(cls):
        cls.divida = Divida.objects.get(pk=2)
        cls.api_key = APIKey.objects.get(pk=1)

    def setUp(self):
        self.factory = test.APIRequestFactory()

    def test_get_divida_user_anon(self):
        # TODO: No futuro não permitir
        data = DividaSerializer(self.divida).data
        request = self.factory.get(reverse(self.url_name, kwargs={'pk':data['id']}))
        view = DividaViewSet.as_view({'get': 'retrieve'})

        response = view(request, pk=data['id'])

        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_get_divida_with_api_key(self):
        data = DividaSerializer(self.divida).data
        request = self.factory.get(reverse(self.url_name, kwargs={'pk':data['id']}), headers={'x-api-key': 'dddd'})
        view = DividaViewSet.as_view({'get': 'retrieve'})

        response = view(request, pk=self.divida.id)

        self.assertEquals(response.status_code, status.HTTP_200_OK)
        self.assertTrue(response.data)


class AddDividaAPI(test.APITestCase):

    fixtures = ['data.json']
    url_name =  'api:dividas-list'

    @classmethod
    def setUpTestData(cls):
        cls.divida = Divida.objects.get(pk=2)
        cls.api_key = APIKey.objects.get(pk=1)

    def setUp(self):
        self.divida_data = {
            'valor': 200,
            'sobre': 'qqqq',
            'devedor': {
                'cpf': '17666666666'
            }
        }
        self.factory = test.APIRequestFactory()

    def test_add_divida_with_api_key(self):
        view = DividaViewSet.as_view({'post': 'create'})
        request = self.factory.post(reverse(self.url_name), self.divida_data)

        test.force_authenticate(request, token=self.api_key)

        response = view(request)

        self.assertEquals(response.status_code, status.HTTP_201_CREATED)

    def test_add_divida_without_api_key(self):
        view = DividaViewSet.as_view({'post': 'create'})
        request = self.factory.post(reverse(self.url_name), self.divida_data)

        response = view(request)

        self.assertEquals(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEquals(response.data, ['Cobradora da dívida não foi específicada.'])

    def test_add_divida_without_payload(self):
        view = DividaViewSet.as_view({'post': 'create'})
        request = self.factory.post(reverse(self.url_name))

        test.force_authenticate(request, token=self.api_key)

        response = view(request)

        self.assertEquals(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_add_divida_sem_valor(self):
        del self.divida_data['valor']
        view = DividaViewSet.as_view({'post': 'create'})
        request = self.factory.post(self.url_name, self.divida_data)

        test.force_authenticate(request, token=self.api_key)

        response = view(request)

        self.assertEquals(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEquals(response.data, {'valor': ['Este campo é obrigatório.']})

    def test_add_divida_sem_devedor(self):
        del self.divida_data['devedor']
        view = DividaViewSet.as_view({'post': 'create'})
        request = self.factory.post(reverse(self.url_name), self.divida_data)

        test.force_authenticate(request, token=self.api_key)

        response = view(request)

        self.assertEquals(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEquals(response.data, {'devedor': ['Este campo é obrigatório.']})
