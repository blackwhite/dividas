from django.test import TestCase, RequestFactory
from django.urls import reverse
from dividasapi.filters import DividaFilter
from dividasapi.models import Divida


class DividaFilterTest(TestCase):

    fixtures = ['data.json']

    def setUp(self):
        self.factory = RequestFactory()

    def test_filter_by_cpf(self):
        request = self.factory.get(reverse('api:dividas-list'), {'cpf': '66666666666'})
        filter_ = DividaFilter(request.GET, queryset=Divida.objects.all())

        self.assertEquals(filter_.qs.count(), 2)

    def test_filter_by_invalid_cpf(self):
        request = self.factory.get(reverse('api:dividas-list'), {'cpf': 'xxxxx'})
        filter_ = DividaFilter(request.GET, queryset=Divida.objects.all())

        self.assertEquals(filter_.qs.count(), 0)
