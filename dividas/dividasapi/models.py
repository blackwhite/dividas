import os
from binascii import hexlify
from decimal import Decimal
from django.db import models
from django.core.validators import MinValueValidator


class Empresa(models.Model):

    nome = models.CharField('Nome', max_length=100)

    def __str__(self):
        return self.nome


class Devedor(models.Model):

    cpf = models.CharField('cpf', max_length=11)

    def __str__(self):
        return self.cpf

    class Meta:
        indexes = [
            models.Index(fields=['cpf'])
        ]


class Divida(models.Model):

    valor = models.DecimalField('Valor', max_digits=12, decimal_places=2,
                                validators=[MinValueValidator(Decimal('1.0'))]
                                )

    sobre = models.TextField('Sobre a dívida', max_length=300, null=True)
    incluida_em = models.DateTimeField('Incluída em', auto_now_add=True)
    atualizada_em = models.DateTimeField('Atualizada em', auto_now=True)

    cobradora = models.ForeignKey(Empresa, on_delete=models.CASCADE)
    devedor = models.ForeignKey(Devedor, on_delete=models.CASCADE)


class APIKey(models.Model):

    api_key = models.CharField(max_length=37)

    empresa = models.ForeignKey(Empresa, on_delete=models.CASCADE)

    @classmethod
    def gerar(cls):
        return hexlify(os.urandom(36)).decode()
