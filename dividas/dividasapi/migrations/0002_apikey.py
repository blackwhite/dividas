# Generated by Django 2.0.1 on 2018-02-22 18:03

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('dividasapi', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='APIKey',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('api_key', models.CharField(max_length=37)),
                ('empresa', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='dividasapi.Empresa')),
            ],
        ),
    ]
