from django.core.exceptions import ObjectDoesNotExist
from rest_framework import authentication
from dividasapi.models import APIKey


class APIKeyAuth(authentication.BaseAuthentication):

    def authenticate(self, request):
        api = None
        api_key = request.META.get('HTTP_X_API_KEY', None)

        if not api_key:
            return None, None

        try:
            api = APIKey.objects.get(api_key=api_key)
        except ObjectDoesNotExist:
            return None, None

        return (None, api)

    def authenticate_header(self, request):
        return 'API KEY inválida'
