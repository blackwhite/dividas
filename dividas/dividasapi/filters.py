import django_filters
import dividasapi.models as models


class DividaFilter(django_filters.FilterSet):

    cpf = django_filters.CharFilter(name='devedor__cpf')

    class Meta:
        model = models.Divida
        fields = ['cpf']
