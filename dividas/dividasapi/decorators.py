from django.http import JsonResponse
from dividasapi.models import APIKey


def api_key_required(func):
    """
    Decorator para permitir o acesso à view somente com uma api key
    """
    def wrap(*args, **kwargs):
        request = args[0]

        if not 'HTTP_X_API_KEY' in request.META:
            msg = {
                'mensage': 'x-api-key não encontrado'
            }
            return JsonResponse(msg, status=401)

        api_key = request.META['HTTP_X_API_KEY']

        api_key_inst = APIKey.objects.filter(api_key=api_key).first()

        if not api_key_inst:
            msg = {
                'mensage': 'API KEY inválida'
            }
            return JsonResponse(msg, status=401)

        request.api_key = api_key_inst

        return func(*args, **kwargs)

    return wrap
