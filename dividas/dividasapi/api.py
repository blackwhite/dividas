import django_filters.rest_framework
from rest_framework import viewsets
from rest_framework import serializers
from dividasapi.models import Divida
from dividasapi.filters import DividaFilter
from dividasapi.serializers import DividaSerializer
from dividasapi.permissions import IsOwnerAdminOrReadOnly


class DividaViewSet(viewsets.ModelViewSet):
    queryset = Divida.objects.all()
    serializer_class = DividaSerializer
    permission_classes = (IsOwnerAdminOrReadOnly,)
    filter_backends = (django_filters.rest_framework.DjangoFilterBackend,)
    filter_class = DividaFilter


    def perform_create(self, serializer):
        if hasattr(self.request, 'auth') and self.request.auth and self.request.auth.empresa:
            serializer.save(cobradora=self.request.auth.empresa)
        else:
            raise serializers.ValidationError('Cobradora da dívida não foi específicada.')
