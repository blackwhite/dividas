from django import forms
from dividasapi.models import Divida, Empresa, Devedor
from django.core.exceptions import ObjectDoesNotExist

class ConsultaCPF(forms.Form):

    cpf = forms.CharField(label='CPF', min_length=11, max_length=11,
                          widget=forms.TextInput(attrs={'class': 'input cpf', 'placeholder':'Digite o seu CPF'}))


class DividaForm(forms.ModelForm):

    cpf = forms.CharField(label='CPF', min_length=11, max_length=11,
                          widget=forms.TextInput(attrs={'class': 'input cpf'}))

    cobradora = forms.ModelChoiceField(queryset=Empresa.objects.all())

    def __init__(self, *args, **kwargs):
        super(DividaForm, self).__init__(*args, **kwargs)

        self.fields['valor'].widget.attrs.update({'class': 'input'})
        self.fields['sobre'].widget.attrs.update({'class': 'textarea'})
        self.fields['cobradora'].widget.attrs.update({'class': 'select'})

        try:
            self.fields['cpf'].widget.attrs.update({'value': self.instance.devedor.cpf})
        except ObjectDoesNotExist:
            pass

    class Meta:
        model = Divida
        fields = ['valor', 'sobre']


    def save(self, commit=True):
        cpf = self.cleaned_data['cpf']
        devedor = Devedor.objects.filter(cpf=cpf).first()

        if not devedor:
            devedor = Devedor(cpf=cpf)
            devedor.save()

        self.instance.devedor = devedor
        self.instance.cobradora = self.cleaned_data['cobradora']

        return super(DividaForm, self).save()
