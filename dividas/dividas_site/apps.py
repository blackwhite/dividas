from django.apps import AppConfig


class DividasSiteConfig(AppConfig):
    name = 'dividas_site'
