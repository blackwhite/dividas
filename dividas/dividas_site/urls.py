"""dividas URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, include
from django.contrib.auth import views as auth_views
import dividas_site.views as site_views

app_name = 'dividas_site'

urlpatterns = [
    path('', site_views.index, name='index'),
    path('search/', site_views.search, name='search'),
    path('admin/', site_views.admin_index, name='admin'),
    path('admin/dividas/<int:id>', site_views.admin_divida_edit, name='admin_divida_edit'),
    path('admin/dividas/<int:id>/deletar', site_views.admin_divida_deletar, name='admin_divida_deletar'),
    path('admin/dividas/add', site_views.admin_divida_add, name='admin_divida_add'),
    path('admin/entrar', auth_views.login, {'template_name': 'site/login.html'}, name='login'),
    path('admin/sair', auth_views.logout, name='logout')
]
