from django.shortcuts import render, redirect, get_object_or_404
from django.views.decorators.http import require_GET
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from dividasapi.models import Divida
from dividasapi.forms import ConsultaCPF, DividaForm


@require_GET
def index(request):
    context = {}

    context['form'] = ConsultaCPF()

    return render(request, 'site/index.html', context)


@require_GET
def search(request):
    context = {}
    form = ConsultaCPF(request.GET or None)

    if form.is_valid():
        cpf = form.cleaned_data['cpf']

        dividas = Divida.objects.filter(devedor__cpf=cpf)
        context['dividas'] = dividas
        context['cpf'] = cpf

        return render(request, 'site/dividas.html', context)

    return redirect('site:index')

@login_required
def admin_index(request):
    context = {}

    dividas = Divida.objects.all()
    context['dividas'] = dividas

    return render(request, 'site/admin.html', context)

@login_required
def admin_divida_add(request):
    context = {}
    form = DividaForm(request.POST or None)

    if form.is_valid():
        divida = form.save()
        messages.success(request, 'Dívida adicionada com sucesso')

        return redirect('site:admin')

    context['form'] = form

    return render(request, 'site/admin_divida_add.html', context)

@login_required
def admin_divida_edit(request, id):
    context = {}

    divida = get_object_or_404(Divida, pk=id)

    form = DividaForm(request.POST or None, instance=divida, initial={'cobradora': divida.cobradora.pk})

    if form.is_valid():
        divida = form.save()
        messages.success(request, 'Dívida atualizada com sucesso')

    context['form'] = form

    return render(request, 'site/admin_divida_edit.html', context)

@login_required
def admin_divida_deletar(request, id):
    divida = get_object_or_404(Divida, pk=id)

    divida.delete()

    messages.success(request, 'Dívida deletada com sucesso')

    return redirect('site:admin')
