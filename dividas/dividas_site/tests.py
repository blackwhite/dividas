from django.test import TestCase


class IndexViewTest(TestCase):

    def test_index(self):
        resp = self.client.get('/')

        self.assertEquals(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'site/index.html')
        self.assertContains(resp, 'Nome sujo? Verifique agora mesmo!')

    def test_context_form(self):
        resp = self.client.get('/')

        self.assertTrue('form' in resp.context)

    def test_method_not_allowed(self):
        resp = self.client.post('/', follow=True)

        self.assertEquals(resp.status_code, 405)


class SearchViewTest(TestCase):

    def test_search(self):
        resp = self.client.get('/search', {'cpf': '00000000000'}, follow=True)

        self.assertEquals(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'site/dividas.html')

    def test_redirect_invalid_cpf(self):
        resp = self.client.get('/search', {'cpf': '111'}, follow=True)

        self.assertRedirects(resp, '/', status_code=301, target_status_code=200)
