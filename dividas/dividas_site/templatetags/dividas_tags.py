import locale
from django import template


locale.setlocale(locale.LC_ALL, 'pt_BR.utf8')
register = template.Library()


@register.filter
def formatar_moeda(value):
    return locale.currency(value)
