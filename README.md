# Dívidas

Um pequeno e simples software de cadastro de dívidas.

Stack:

* [python](https://www.python.org/)
* [django](https://www.djangoproject.com/)
* [bulma](https://bulma.io/)

## Imagens

![](https://bytebucket.org/blackwhite/dividas/raw/10eef3177e6cd2e36647459ddaa198b059c36784/images/index.png)

![](https://bytebucket.org/blackwhite/dividas/raw/10eef3177e6cd2e36647459ddaa198b059c36784/images/resultado_consulta_cpf.png)

![](https://bytebucket.org/blackwhite/dividas/raw/10eef3177e6cd2e36647459ddaa198b059c36784/images/admin_index.png)

## TODO

* projeto específico para a API
* app "site" consultar a API
